package com.classpath.mockdataproducer.model;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
public class User {
    private long id;
    private String name;
    private int age;
}
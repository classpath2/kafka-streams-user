package com.classpath.mockdataproducer.service;

import com.classpath.mockdataproducer.model.User;
import com.classpath.mockdataproducer.producer.MockNameProducer;
import com.classpath.mockdataproducer.serdes.StreamsSerdes;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import java.util.Properties;

@Component
public class UserStreamingApp implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {

        //Key
        Serde<String> stringSerde = Serdes.String();

        //Value
        Serde<User> userSerde = StreamsSerdes.UserSerde();

        //1. Stream builder
        StreamsBuilder streamsBuilder = new StreamsBuilder();

        //building the topology
        /*KStream<String, User> inputStream = streamsBuilder.stream("source-topic", Consumed.with(stringSerde, userSerde));

        KStream<String, User> seniorCitizenUserStream = inputStream.filter(((userId, user) -> user.getAge() > 60));

        KStream<String, String> nameStream = seniorCitizenUserStream.mapValues(user -> user.getName());

        nameStream.to("senior_citizen_names", Produced.with(stringSerde, stringSerde));
        */

        KStream<String, User> ks01 = streamsBuilder.stream("users-topic", Consumed.with(stringSerde, userSerde));

        ks01.filter(((userId, user) -> user.getAge() >= 60))
                      .mapValues(user -> User.builder().name(user.getName()).age(user.getAge()).id(user.getId()).build())
                      .to("senior_citizen_citizens", Produced.with(stringSerde, userSerde));

        ks01.filter(((userId, user) -> user.getAge() < 60))
                      .mapValues(user -> User.builder().name(user.getName()).age(user.getAge()).id(user.getId()).build())
                      .to("adults_names", Produced.with(stringSerde, userSerde));

        KafkaStreams kStreams = new KafkaStreams(streamsBuilder.build(), getProperties());

        MockNameProducer.generateTestData();
        //starting the stream
        kStreams.start();

        //closing the stream
        Thread.sleep(50000);
        kStreams.close();
    }

    private static Properties getProperties() {
        Properties props = new Properties();
        props.put(StreamsConfig.CLIENT_ID_CONFIG, "Senior-Citizen-App-Client");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "user-filter");
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "Kafka-Streams-App");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "157.245.108.69:9092");
        return props;
    }
}
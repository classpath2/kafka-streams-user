package com.classpath.mockdataproducer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MockDataProducerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MockDataProducerApplication.class, args);
    }

}

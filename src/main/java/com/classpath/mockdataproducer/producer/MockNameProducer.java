package com.classpath.mockdataproducer.producer;

import com.classpath.mockdataproducer.config.AppConfig;
import com.classpath.mockdataproducer.model.User;
import com.classpath.mockdataproducer.serializer.JsonSerializer;
import com.github.javafaker.Faker;
import com.github.javafaker.Name;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;


import java.util.Properties;

public class MockNameProducer {
    public static void generateTestData() {
        Properties props = new Properties();
        props.put(ProducerConfig.CLIENT_ID_CONFIG, AppConfig.applicationID);
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, AppConfig.bootstrapServers);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);

        KafkaProducer<String, User> producer = new KafkaProducer<>(props);

        Faker faker = new Faker();
        for (int i = 1; i <= AppConfig.numEvents; i++) {
            Name fakeName = faker.name();
            User user = User.builder()
                    .name(fakeName.fullName())
                    .age(faker.number().numberBetween(55, 65))
                    .id(faker.number().randomNumber())
                    .build();
            producer.send(new ProducerRecord<>(AppConfig.topicName, user.getName()+"-"+user.getId(), user));
        }

    }



}
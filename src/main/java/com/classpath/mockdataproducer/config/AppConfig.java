package com.classpath.mockdataproducer.config;

public class AppConfig {

    public final static String applicationID = "Streaming-app";
    public final static String bootstrapServers = "157.245.108.69:9092";
    public final static String topicName = "users-topic";
    public final static int numEvents = 1000;

}